VIEWS DEFAULT ARGUMENTS
---------
The Views Default Arguments module allows provide additional default arguments for views contexture filter.


Features
--------------------------------------------------------------------------------
* Detect entity reference field as configured in current active node and pass them as input arguments of views contexture filter. User
could also choose to hide view if entity reference field or its value are not available.


Requirements
--------------------------------------------------------------------------------
Views


Install
--------------------------------------------------------------------------------
- Enable Views Default Arguments
- Add a CONTEXTUAL FILTERS to your view display
- Choose "Node ID" and confirm
- Click "WHEN THE FILTER VALUE IS NOT AVAILABLE" then select "Provide default value"
- Select "Content ID from Node Entity Reference" from "Type" list options
- Enter the name of entity reference field in "Entity Reference" text field
- Check "Hide view if Entity Reference does not available" if you do want to hide the view display when entity reference field or values are not available.
- Save your view and enjoy!

Credits / Contact
--------------------------------------------------------------------------------
Created by Keen WU<xiaoluan0318@hotmail.com>
